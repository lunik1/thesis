s/Z\+jets/\\Zjets\{\}/;
s/tt(?=})/\\ttbar/;
s/V{2,3}/\\$&/;
s/ttV/\\ttV/;
s/NPL/\\textls{NPL}/;
s/tZq/\\tZq/;
s/ \(GeV\)/\\rmfamily{} (\\si{\\GeV})/
