#!/bin/env perl

use strict;
use warnings;

while (<>) {
    s/\\Delta/\\Δ/g;
    s/ Impact\$/\$ Impact/;
    s/\$\\color\[1]\{(.+?)\}\$/$1/;
    s/_rate/ normalisation/;
    s/b tag/\\btag{}/;
    s/NPL/\\textls{NPL}/;
    s/DYToLL\b/\\Zjets{}/;
    s/(?<={)scale/\\μF{} and \\μR{} scale (\\textls{ISR} and \\textls{FSR})/;
    s/isr/\\μF{} and \\μR{} scale (\\textls{ISR})/;
    s/fsr/\\μF{} and \\μR{} scale (\\textls{FSR})/;
    s/TbartChan\b/\\Ptb\\Pq{} (t-channel)/;
    s/TtChan\b/\\Pt\\Pq{} (t-channel)/;
    s/TsChan\b/\\Pt\\Pq{} (s-channel)/;
    s/TTG\b/\\ttγ{}/;
    s/(?<={)[W,Z,G]{2,3}\b/\\$&\{\}/;
    s/TTW\b/\\ttW{}/;
    s/TWZ\b/\\tWZ{}/;
    s/TTZ\b/\\ttZ{}/;
    s/TT\b/\\ttbar{}/;
    s/TW\b/\\tW{}/;
    s/TbarW\b/\\tbarW{}/;
    s/ttH\b/\\ttH{}/;
    s/PDF/\\textls{PDF}/;
    s/pileup/Pileup/;
    s/JECs/\\textls{JEC}s/;
    s/\(ee\)/(\\ee)/;
    s/\(\\mu\\mu\)/(\\μμ)/;
    s/ME/\\textls{ME}/;
    s/hdamp/\\textls{ME}--\\textls{PS} matching scale (\$h_{\\text{damp}}\$)/;

    s/lumi_(201[67])/Luminosity ($1)/;
    s/bTag_(201[67])/\\btag{}ging ($1)/;
    s/jer_(201[67])/Jet smearing ($1)/;
    s/pdf_(201[67])/\\textls{PDF} ($1)/;
    s/jes_(201[67])/\\textls{JEC}s ($1)/;
    s/met_(201[67])/\\mET{} ($1)/;
    s/trig_(201[67])/Lepton efficiency ($1)/;
    s/fake_ee normalisation_(201[67])/\\textls{NPL} contribution (\\ee, $1)/;
    s/fake_mumu normalisation_(201[67])/\\textls{NPL} contribution (\\μμ, $1)/;

    print unless /CMS/ or /Internal/;
}

exit;
