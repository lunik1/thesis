%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/conclusion/}{../fig/conclusion/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/conclusion/}{fig/conclusion/}}
\makeatother

\begin{document}

\chapter{Discussion and Conclusion}\label{chap:conclusion}

\epigraph{\textit{A witty saying proves nothing}}{\textsc{Voltaire}}

\section{Other Searches for tZq}\label{sec:other_searches}

No search for \tZq{} in the dilepton final state has been published by \ac{cms}
or any other physics experiment. Four previous searches for \tZq{} in the
trileptonic final state have been published, all from experiments situated on
the \ac{lhc}. Three of these searches were published by the \ac{cms}
Collaboration. The first of these was conducted on \SI{19}{\invfb} of
\(√{s}=\SI{8}{\TeV}\) collision data from the 2012 dataset, a signal strength
of \(1.22_{-0.85}^{+0.98}\) was measured with a corresponding observed
(expected) significance of~\(2.4σ\) (\(1.8σ\))~\cite{tzq_run1}. The second
search at \ac{cms} used \SI{36.9}{\invfb} of \(√{s}=\SI{13}{\TeV}\) collision
data from the 2016 data-taking period and observed a signal strength
of~\(1.31_{-0.33}^{+0.35}\text{(stat)}_{-0.25}^{+0.31}\text{(syst)}\) with an
observed (expected) significance of \(3.7σ\) (\(3.1σ\))~\cite{tzq_cms}. The
third published search for \tZq{} at \ac{cms} used \SI{77.4}{\invfb} of
\(√{s}=\SI{13}{\TeV}\) collision data from the 2016–2017 data-taking period; it
measured a signal strength of
\(1.18_{-0.13}^{+0.14}\text{(stat)}_{-0.10}^{+0.11}\text{(syst)}_{-0.04}^{+0.04}\text{(theo)}\)
with an observed (expected) significance of \(8.2σ\)
(\(7.7σ\))~\cite{tzq_cms2}. The final published \tZq{} analysis comes from the
\ac{atlas} collaboration, performed on \SI{36.1}{\invfb} of
\(√{s}=\SI{13}{\TeV}\) collision data from 2015–2016. A signal strength of
\(0.75±0.28\) was observed, with corresponding observed (expected) significance
of~\(4.2σ\) (\(5.4σ\))~\cite{tzq_atlas}.

All measurements of the \tZq{} cross section in the trilepton final state are
compatible with the \ac{sm} prediction. The observed and expected cross sections
reported by the trileptonic \tZq{} analyses are greater than the expected
significance in the dileptonic \tZq{} analysis presented in this thesis,
despite the dileptonic decay channel having a greater predicted cross section.
This can be attributed the profile of the backgrounds in the two channels. In
the trilepton channel, the greatest contributors to the background are the
\WZ{} and \ttZ{} processes. The contribution of the two largest backgrounds to
the dileptonic search, the \Zjets{} and \ttbar{} processes, is greatly reduced
due to the three lepton requirement: an additional \ac{npl} is required in
these processes in order to have the required number of leptons in the final
state. The \WZ{} and \ttZ{} processes have smaller cross sections relative to
\tZq{} than \Zjets{} and \ttbar, allowing \tZq{} events to be more easily
isolated. This allows  for the improved performance over the dilepton analysis.

\section{Summary of the tZq Analysis}

A search was performed for the production of a \PZ~boson in association with a
single top quark. Following from previous observations in the trilepton final
state, the analysis presented in this thesis attempted to observe the \tZq{}
process in the dilepton final state using \SI{78}{\invfb} of data obtained
by the \ac{cms} experiment in the 2016–2017 data-taking period.

A selection procedure was devised in order to isolate \tZq{} events, selecting
events with exactly two opposite-sign same-flavour leptons compatible with the
nominal \PZ~boson mass. In addition, these events required \numrange{4}{6}
jets, \numrange{1}{2} of which must be \btag{}ged, and a pair of which must be
compatible with the nominal \PW~boson mass (with a veto on the leading
\btag{}ged jet). \Acp{cr} enriched in \Zjets{} and \ttbar{}
production were created. Agreement between data and simulated samples in these
regions lay within the combined statistical and systematic uncertainties.

The purity of the signal region is low, and so \ac{ml} techniques were used to
identify \tZq{} events. \acp{gp} were used to automate the hyperparameter
selection procedure for \acp{bdt}; hyperparameter choices that resulted in
well-performing classifiers were found within 100 iterations.  The performance
of these automatically optimised \acp{bdt} compared favourably to \acp{mlp}
optimised by hand.

The expected signal strength across the full dataset is
\(\hat{r}=6.52_{-2.05}^{+2.30}\) with an observed (expected) significance of
\(3.12σ\) (\(0.48σ\)). This may indicate excess of \tZq{} process in the
dilepton channel over the \ac{sm} prediction, however \(\hat{r}=1\) still lies
within a \(3σ\) envelope. The observed significance far exceeds the expected
significance because of the large value of \(\hat{r}\)\,: the expected
significance was determined assuming \(\hat{r}=1\).

% This is consistent
% with the background-only hypothesis. If unblinded, the analysis is unlikely
% to provide evidence of the \tZq{} process in the dilepton channel.

\section{Future Work}\label{sec:future_work}

As mentioned in \cref{sec:mlp_hyperparameter_selection}, one improvement that
can be made to the analysis is the application of the automatic hyperparameter
selection to the \ac{mlp} classifiers. A possible workaround to the known
challenges would to be exclude qualitative hyperparameters and the number of
hidden layers from the optimisation procedure. Multiple optimisations could
then be performed with the excluded hyperparameters at fixed values.
Alternatively, an emerging method of hyperparameter optimisation is to use
genetic programming~\cite{tpot}. In this method, multiple different classifiers
are evaluated. The best-performing are then mutated and bred (i.e.\ properties
from multiple classifiers are combined) to form a new generation, where the
process repeats. The advantage of such a method is not only the ability to
easily include qualitative hyperparameters, but also allowing both feature
preprocessing and feature selection to be incorporated into the automatic
optimisation procedure.

The statistical uncertainty in the \tZq{} analysis can, of course, be reduced
by including more data. Work is currently under way to encompass the full
\runII{} dataset (i.e.\ include data from the 2018 data-taking period).
Ideally, including 2018~data will resolve the question of whether an excess in
the \tZq{} process exists by reducing or increasing the tension between the
observed signal strength and \ac{sm} prediction.

\begin{figure}
	\centering \includegraphics[width=\textwidth]{btaggers_roc.pdf}
	\caption{\glsfmtshort{roc} comparing the performance of the
		\glsfmtshort{deepcsv} \btag{}ging algorithm with the older
		\glsfmtshort{csvv2} and c\textls{MVA}v\case{2} \btag{}ging algorithms.
		Performance was measured on \case{\textls{AK4}} jets of
		\(\pT>\SI{30}{\GeV}\) in 2016 \ttbar{} events and is shown separately
		for charm and light (\Pu\Pd\Ps\Pg)
	jets. Figure taken from~\cite{btag_roc}.}\label{fig:btaggers_roc}
\end{figure}

One change that must be made to the analysis as part of utilising the full
\runII{} dataset is a change of \btag{}ging algorithm. \Ac{csvv2} has been
retired for use in~data taken in 2018 and beyond, replaced by the \Ac{deepcsv}
\btag{}ging algorithm~\cite{deepcsv,btag}. \Ac{deepcsv} uses deep neural
networks to achieve an improved classification performance; the \acp{roc} of
\ac{csvv2} and \ac{deepcsv} are compared in \cref{fig:btaggers_roc}. Replacing
\ac{csvv2} with \ac{deepcsv} should help further distinguish \tZq{} events
backgrounds without a real \Pb~jet across the whole \runII{} dataset, as it
can be applied retroactively.

An area that could be improved with respect to reducing the dominant \Zjets{}
background is the \PW~boson reconstruction. As can be seen in
\cref{fig:cutflow_2016,fig:cutflow_2017}, the \PW~mass cut retains much of the
\Zjets{} background, despite it not containing a real \PW~boson. A possible
improvement would be to closely investigate the properties of jets that
originate from \PW~boson decay and determine if they can be distinguished from
jets that do not. With this information, events that do not contain two jets
identified as potentially originating from a \PW~boson decay within a \PW~boson
mass window could be rejected. Properties of jet pairs outside of their
invariant mass could also be considered. If \PW~boson reconstruction can be
improved, a widening of the \PW~boson mass cut window could be considered, as
\cref{fig:cutflow_2016,fig:cutflow_2017} suggest that \tZq{} events could be
recovered by doing so (currently, due to the \Zjets{} background, no advantage
was found in widening the \PW~mass window). This must be done with care to
avoid starving the \Zjets{} \ac{cr}, or a new \Zjets{} \ac{cr}
may need to be defined.

The latest \tZq{} trilepton analysis~\cite{tzq_cms2} made use of \ac{ml}
classification to identify \acp{npl}. \acp{npl} are a much larger contributor
to the background in the trilepton analysis but nevertheless implementing this
in the \tZq{} analysis should improve the purity of the signal region, albeit
only  slightly.

The \tZq{} analysis suffers from large systematic uncertainties, and efforts
made to constrain these may prove fruitful. A simultaneous fit in the signal
and \acp{cr} is a possible way to achieve this—it would help to
constrain the uncertainty associated with the \Zjets{} and \ttbar{}
normalisation. It could also be possible to exclude regions in which systematic
uncertainties are particularly large.

\end{document}
