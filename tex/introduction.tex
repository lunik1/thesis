%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/introduction/}{../fig/introduction/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/introduction/}{fig/introduction/}}
\makeatother

\begin{document}

\chapter{Introduction}

\epigraph{\textit{In all things of nature there is something of the marvellous.}}{\textsc{Aristotle}}

\lettrine[lines=5]{I}{t}\label{initial} was \textsc{Leucippus}, according to
\textsc{Aristotle}, who first promoted atomism in the Western
canon~\cite{greek_philosophy}. The sage \textsc{Aruni} had espoused similar
ideas in India c.~800~\textls{BCE}, three centuries
prior~\cite{McEvilley:2002}. The concept behind atomism is simple and, in the
modern day, familiar: the macroscopic world with which we interact every day is
underpinned by a microscopic world of imperceptibly small, indivisible
atoms—the fundamental building blocks of everything. Of course, the atom of
early atomism with its infinite possible shapes and sizes was not the atom of
\textsc{Dalton}, where each element consisted of an atom of a unique type. Nor
was, as \textsc{Thomson} would find in his investigations into cathode rays in
1897~\cite{electron}, the atom itself fundamental. There existed a smaller
unit: the electron. \textsc{Rutherford} would observe that these electrons
orbited a dense nucleus containing a positively charged particle: the
proton~\cite{Rutherford:1920}. Later, the remaining component of the nucleus,
the uncharged neutron, would be observed by \textsc{Chadwick}~\cite{neutron}.
Over time, further fundamental particles were discovered. The muon in
1937~\cite{muon}. The electron neutrino in 1956~\cite{electron_neutrino}. The
muon neutrino in 1962~\cite{muon_neutrino}. In 1969 the atom itself was further
divided: not only did the nucleus consist of protons and neutrons, but protons
and neutrons themselves contained indivisible point
particles~\cite{quarks1,quarks2}. These were later dubbed the up and down
\emph{quark}. By the discovery of the \(\Pτ\)~lepton in
1975~\cite{tau_discovery}, a truly remarkable theory was in development. A
theory that could unite the expanding particle zoo, codifying their
relationships and interactions via the fundamental \emph{electromagnetic},
\emph{strong nuclear}, and \emph{weak nuclear} forces. Today, we know this
theory as the \ac{sm}.

An impressive predictive power—such as the foretelling the discovery of the
\PWpm~bosons (1983)~\cite{wz_discovery}, \(\PZ⁰\)~boson
(1983)~\cite{wz_discovery}, and tau neutrino
(2000)~\cite{tau_neutrino}—cemented the Standard Model as the standard model of
modern particle physics. But, as~the field developed, it became clear that the
\ac{sm} could not be a complete theory of matter. Where was the mechanism
of gravity? Why is there so little antimatter in the Universe? What is dark
matter? These are all open questions to which the \ac{sm} provides no answer.
Paradoxically, the predictive power of the \ac{sm} never seemed to falter, its
arguably crowing achievement arriving in 2012 when the \ac{cms} and \ac{atlas}
experiments discovered the long-theorised Higgs
Boson~\cite{Aad:2012tfa,Chatrchyan:2012xdj}. An overarching goal of particle
physics today is therefore to find where the predictions of the \ac{sm} are
amiss and in doing so understand the shape of the physics that lies beyond it.
A physics in which the four fundamental forces can be unified, the
matter–antimatter asymmetry is justified, and the ingredients of dark matter
are known. Fortunately, in a \SI{27}{\km} tunnel below the Franco–Swiss
countryside, particle physics has a powerful ally in this quest. The most
powerful and luminous~particle accelerator ever constructed: the \ac{lhc}. The
base of the aforementioned \ac{cms} and \ac{atlas} experiments, the \ac{lhc}
allows the investigation of energy scales where divergence from the \ac{sm} due
to \ac{bsm} physics may be observable. Particle physics is a broad field, and
so the question naturally arises of where efforts to discover these divergences
are best served. A compelling candidate, upon which this thesis will focus,
lies in an as-yet unmentioned element of the \ac{sm}: the top quark.

The cynosure of the top quark is its mass; at
\(\SI{173.0±0.4}{\GeV}\)~\cite{PDG} it is the most massive particle in the
\ac{sm}. This grants the top quark some unique properties. Chiefly, unlike the
other quarks, the top quark cannot form hadrons as its lifetime is shorter than
the time scale of the hadronisation process. The properties of the top quark
can, consequently, be probed more directly than other quarks. This thesis
explores a rare process involving the top quark predicted by the \ac{sm}:
single top production in association with a \(\PZ⁰\)~boson (\tZq). The \tZq{}
process is sensitive not only to the couplings of the top quark, but also the
couplings between the \(\PZ⁰\)~and \PWpm~bosons, making an excellent probe of
\ac{sm} predictions in these sectors.

A search for the \tZq{} process in the dilepton channel—i.e.\ following a
leptonic decay of the \(\PZ⁰\)~boson and a hadronic decay of the \PWpm~boson
produced by the~decaying top quark—at the \ac{cms} experiment is presented.
\cref{chap:physics} explores in more detail the \ac{sm} and top physics
specifically. \cref{chap:ml} provides background on the \acl{ml} techniques
used in the analysis to extract the \tZq{} signal. The \ac{lhc} and \ac{cms}
detector are described in \cref{chap:detector}. \cref{chap:reco,chap:sim}
describe the reconstruction and simulation of proton–proton collisions at the
\ac{cms} detector, and \cref{chap:selection}~the selection procedure applied to
these events in the \tZq{} analysis. Sources of systematic uncertainty are
covered in \cref{chap:systematics}. \cref{chap:results} presents the results of
the analysis, these are discussed and summarised in \cref{chap:conclusion}.

\end{document}
