%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/reco/}{../fig/reco/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/reco/}{fig/reco/}}
\makeatother

\begin{document}

\chapter{Event Simulation}\label{chap:sim}

\epigraph{\textit{Essentially, all models are wrong, but some are useful.}}{\textsc{George Box}}

Analyses at the \ac{cms} experiment routinely incorporate simulated data
generated using \ac{mc} techniques~\cite{mc}. This not only allows the
optimisation of a search while blinded to the real data, but also allows access
to the \emph{\ac{mc} truth}. Truth values describe events at a generator level
(i.e.\ the level of fundamental particles) rather than at detector level (i.e.\
following \ac{cms}'s reconstruction). This information is essential, for
example, when evaluating the performance of a \ac{ml} classifier. Simulated
samples are often central to an analysis' methodology, so it is crucial they
reflect reality as accurately and in as much detail as possible. This chapter
discusses the techniques and software used to generate the simulated samples
used at \ac{cms}\@.

\section{Event Generation}\label{sec:event_generation}

The generation of a simulated event can be split, in general terms, into three
stages: the initial hard proton–proton interaction, the subsequent parton
shower and hadronisation, and the \ac{cms} detector system's response.
Typically, different specialist software packages that interface with one
another are used for each step, the final result being a simulated event with
generator-level truth information and simulated response at the detector level.

Across all the simulated samples used in the \tZq{} analysis (listed later in
\cref{sec:samples}), three different \emph{event generators} are used to
replicate the proton–proton interaction: \madgraph~\cite{madgraph},
\ac{amcatnlo}~\cite{madgraph}, and \powhegbox{}~\cite{powheg_1, powheg_2,
powheg_3}. \madgraph{} and \ac{amcatnlo} refer to the same generator framework,
known collectively as \madnlo{}, but operating to the tree and one-loop levels,
respectively. Both \madnlo{} and \powhegbox{} sample parton momenta from a
proton \ac{pdf} and use perturbative methods to determine the interactions that
take place. This involves the calculation of the so-called \acp{me}, which
describe the likelihood of transitions to specific final states.

The ensuing parton showering and hadronisation is simulated by \ac{smc}
generators; \pythia~\cite{pythia} fills this role for all simulated samples
used in the \tZq{} analysis\footnote{\pythia{} itself is a full event generator
like \madnlo{} and \powhegbox{}, but only its parton shower simulation
capabilities are used in this case.}. This includes the simulation of gluon
\acl{isr} (\acs{isr}) at the chosen renormalisation scale, \μR, and gluon
\ac{fsr} at the chosen factorisation scale, \μF. \Acp{pdf} are functions of
these energy scales in practice but not in principle: the dependence exists
only at finite levels of perturbation theory. As \μF{} and \μR{} are chosen, a
systematic error is associated with this choice (see
\cref{sec:syst_ME,sec:syst_scale}).

The matching of the particles described in the perturbative \ac{qcd} hard
interaction simulation to those described in the nonperturbative \ac{qcd}
hadronisation simulation is also performed by \pythia{}. Depending on the event
generator, a different matching algorithm is used: \textls{MLM}~\cite{mlm_1,
mlm_2} for \madgraph{}, FxFx~\cite{fxfx} for \ac{amcatnlo}, whilst \powhegbox{}
interfaces with \ac{smc} generators directly. This matching step introduces
another systematic uncertainty, covered in \cref{sec:syst_hdamp}.

% At \ac{nlo} the matching introduces a nontrivial double-counting problem. This
% occurs when \ac{nlo} diagrams at the matrix element simulation stage are
% replicated when combining a LO event with a parton shower, as \pythia{} (and
% other \ac{smc} generators) do not guarantee \ac{nlo}
% accuracy~\cite{powheg_proc}. Two methods have been developed to correct for
% this—\ac{mcatnlo} and \ac{powheg}. The former introduces negatively weighted
% events of a cross section equal to the additive inverse of the cross section
% used by the \ac{smc} generator. By interfacing directly with \ac{smc}
% generators, the \ac{powheg} method bypasses the need for negatively weighted
% events.
%
% In \ac{amcatnlo} samples containing negative event weights, an additional flat
% scale factor is required for correct normalisation. This is
% \begin{equation}
%     SF_{\text{NLO}} = \frac{N}{N_{+}-N_{-}}
% \end{equation}
% where \(N\) is the number of events in the sample, \(N_{+}\) is the number of
% positively weighted events in the sample, and \(N_{-}\) is the number of
% negatively weighted events in the sample (\(N=N_{+}+N_{-}\)). These numbers are
% considered before any event selection is performed.

In a simulated sample, additional minimum-bias events (events that are not
selected to contain certain physics objects but still exceed some minimum
amount of detector activity) generated by \pythia{} are superimposed with
events from the process of interest to imitate the effects of pileup. This
does not exactly replicate the pileup seen in data, and so an additional
reweighting detailed in \cref{sec:pileup_modelling} is applied.

To view simulated events though the lens of the \ac{cms} detector, a simulation
of the \ac{cms} detector itself was created with the \ac{geant4} software
package~\cite{geant4_1, geant4_2, geant4_3, geant4_cms}. The simulation
attempts to replicate interactions of particles with the \ac{cms} detector's
magnetic field and material. A simulated detector readout is created, used to
reconstruct the generated events in the same way as real data, as described in
\cref{chap:reco}.

Any number of events can be generated for a given process, and so a given
simulated sample must be renormalised to reflect the number of events expected
of that process in a data sample corresponding to a given integrated
luminosity. This normalisation factor, \(w\), is given by
\begin{equation}\label{eq:sim_scale}
    w = \frac{\lumi σ}{N}
\end{equation}
where \lumi{} is the integrated luminosity, \(σ\) the cross section of
the process, and \(N\) the number of simulated events.

\section{Simulation Corrections}

Corrections are applied to simulated samples to account for known problems and
rectify observed discrepancies with data. These corrections may alter the
properties of only a subset of observable quantities in an event, e.g.\ jet
\pT{}, or may apply a corrective scale factor on a per-event basis. Most
applied corrections introduce associated systematic uncertainties, covered in
\cref{chap:systematics}.

\subsection{Pileup Modelling}\label{sec:pileup_modelling}

The pileup observed in data is difficult to both predict and model, resulting
in considerable disparity between the number of pileup interactions observed
in data and simulated events. To correct for this, a scale factor is applied to
each simulated event. This scale factor is a function of the number of primary
vertices in an event, \(n_{\text{PV}}\), and uses the \(n_{\text{PV}}\)
distribution in minimum-bias data events as the baseline.

\subsection{Jet Energy Smearing}\label{sec:jet_smearing}

In data, the observed jet energy resolution is \({≈}\SI{10}{\percent}\)
poorer than in simulated samples. To account for this a scale factor,
\(c_{\text{JER}}\), is applied to the four-momentum of jet objects in
simulation in a process known as \emph{jet smearing}.

If a matching generator-level jet can be found, the scale factor is determined
by
\begin{equation}\label{eq:matching_smearing}
    c_{\text{JER}} = 1 + \left(s_{\text{JER}}-1\right)\frac{\pT-\pT^{\text{gen}}}{\pT}
\end{equation}
where \(\pT^{\text{gen}}\) is the \pT{} of the matching generator-level jet and
\(s_{\text{JER}}\) is a \(η\)-dependant scale factor provided by the \ac{cms}
\ac{jerc} subgroup. A detector-level jet is considered to match with a
generator-level jet if and only if
\begin{equation}
    \Δ R < \frac{R}{2}\quad∧\quad\left|\pT-\pT^{\text{gen}}\right| < 3σ_{\text{\pT}}\pT
\end{equation}
where \(σ_{\text{\pT}}\) is the relative \pT{} uncertainty in simulation,
\(R=0.4\) in \case{AK4} jets (see \cref{sec:reco_jets}), and \(\ΔR\) is defined
in \cref{eq:delta_r}. When a matching generator-level jet is not found,
\emph{stochastic smearing} is used. In this case,
\begin{equation}\label{eq:stochastic_smearing}
    c_{\text{JER}} = 1 +
    \symcal{N}\left(0,σ_{\text{\pT}}\right)√{\max(s^{2}_{\text{JER}}-1,0)}.
\end{equation}
The chosen value of \(\symcal{N}\left(0,σ_{\text{\pT}}\right)\) is fixed
for each jet i.e.\ it will remain the same when calculating systematic
variations described in \cref{chap:systematics}.

An error was found in the calculation of \(c_{\text{JER}}\) in the 2016
analysis where the uncertainty in \(s_{\text{JER}}\) was used in place of
\(σ_{\text{\pT}}\) to determine \(c_{\text{JER}}\). This has been fully
remedied in the 2017 analysis but still affects the \μμ{} channel of the 2016
analysis. An attempt was made to mitigate the impact of this error; its
effectiveness is assessed in \cref{sec:smearing_mitigation}.

\subsection{b Tagging Efficiency}\label{sec:b-tag_efficiency}

The \ac{cms} \ac{btv} group is responsible for measuring and comparing the
efficiency and misidentification rates of \btag{}ging algorithms in data and
simulation. This is performed using multijet and \ttbar{} samples. Scale
factors are applied on a per-event basis as functions of jet flavour, \pT{},
and pseudorapidity in order to mitigate any differences seen in \btag{}ging
efficiency between data and simulation.

% more detail? - b tag contact!

\end{document}
