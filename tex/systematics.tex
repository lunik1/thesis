%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/systematics/}{../fig/systematics/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/systematics/}{fig/systematics/}}
\makeatother

\begin{document}

\chapter{Systematic Uncertainties}\label{chap:systematics}

\epigraph{\textit{I beseech you, in the bowels of Christ, think it possible you may be mistaken.}}{\textsc{Oliver Cromwell}}

When performing a physics analysis, attentive account must be kept of the
sources of systematic uncertainty. These can originate from e.g.\ insufficient
understanding of the underlying physics, imperfect modelling in simulated
samples, or limitations of the \ac{cms} detector itself. Careful consideration
of systematic uncertainties is of particular importance in searches for rare
processes such as \tZq, as even systematic uncertainties with a minor impact
may be significant compared to the \tZq{} cross section.

Systematic uncertainties can be split into two categories, depending on their
effects on the underlying distributions of observables. \emph{Rate
uncertainties} affect the overall normalisation of distributions, but do not
otherwise change distributions' shapes. \emph{Shape uncertainties}, in
contrast, change distributions' shapes and may, as a result, affect the overall
normalisation.

The systematic uncertainties described in this chapter are included as nuisance
parameters in the signal extraction fit, described in
\cref{sec:likelihood_model}. To do this, the impact of a \(±1σ\) change in each
systematic uncertainty on the distributions of observables is defined. In many
cases, this involves the use of \emph{per-event weights}, which reweight events
on an individual basis so the overall distributions of observables (shapes)
reflect what is expected from a \(±1σ\) variation for a given source of
systematic uncertainty.

\section{Rate Uncertainties}

\subsection{Integrated Luminosity}\label{sec:syst_lumi} % lumi

The relative uncertainty in the integrated luminosity collected by \ac{cms} in
2016 and 2017 is \SI{±2.5}{\percent} and \SI{±2.3}{\percent},
respectively~\cite{2016_lumi,2017_lumi}. Every simulated sample is scaled to
this luminosity as per \cref{eq:sim_scale}, and so the effect of this
uncertainty is an overall normalisation uncertainty applied equally to all
simulated samples.

\subsection{Cross Section Normalisation} % rate

To account for the uncertainty in the cross sections of simulated samples, a
normalisation uncertainty is applied individually to each background process.
Aping the previous \tZq{} analysis at \ac{cms} in the trilepton channel, this
uncertainty is set at \(±\SI{30}{\percent}\) for each process~\cite{tzq_cms}.

\subsection{Data-Driven Nonprompt Lepton Estimate}

Following the \ttbar{} analysis on which the \ac{npl} estimation strategy was
based~\cite{npl2}, a \SI{30}{\percent} uncertainty in the normalisation of the
data-driven \ac{npl} contribution (see \cref{sec:npl_estimate}) is applied.
This is intended to cover all systematic uncertainties acting on the simulated
samples used in the determination of data-driven \ac{npl} contribution. The
only other systematic uncertainty the \ac{npl} estimate is subject to in the
fit is the uncertainty in luminosity described above.

\subsection{Lepton Efficiencies}\label{sec:lepton_eff_syst}  % trig

Centrally-provided \(±1σ\) variations to the scale factors associated with
lepton identification (see \cref{sec:lepton_selection}) forms one component of
the lepton efficiency systematic uncertainty. The other component incorporates
the uncertainty in the trigger efficiencies (described in
\cref{sec:trigger_efficiency}), which are varied by \(±\SI{1}{\percent}\) in
the \ee{} and \μμ{} channels, and \(±\SI{2}{\percent}\) in the \eμ{} channel to
form the \(±1σ\) variations. This alteration to the trigger efficiencies has
been previously found to account for residual differences between data and
simulation~\cite{Morton19}.

\section{Shape Uncertainties}

\subsection{Jet Energy Corrections}\label{sec:jec_syst}  % jes

As described in \cref{sec:reco_jets}, \acp{jec} are applied to simulated events
so they more closely reflect data. The uncertainties associated with these
corrections are provided by the \ac{cms} \ac{jec} group. In order to evaluate
the impact of this systematic uncertainty, jet properties are recalculated
after varying the \acp{jec} by \({±}1\) standard deviation.

\subsection{Jet Smearing}  % jer

A nuisance parameter associated with the jet smearing procedure described in
\cref{sec:jet_smearing} is formed by altering \(𝑠_{\text{JER}}\) by \(±1σ\) in
the calculation of \(𝑐_{\text{JER}}\)
(\cref{eq:matching_smearing,eq:stochastic_smearing}). This uncertainty in
\(𝑠_{\text{JER}}\) is centrally provided.

\subsection{Missing Transverse Energy}\label{sec:syst_met}  % met

As \mET{} is determined using the total \pT{} of all \ac{pf} objects, the
\acp{jec} and jet smearing have a knock-on effect on its value. This is
accounted for by propagating the \(±1σ\) variations of \acp{jec} and jet
smearing to the \mET{} calculation when those systematic uncertainties are
evaluated (the so-called type~Ⅰ corrections).

An additional systematic uncertainty affecting the \mET{} alone arises from the
contribution of unclustered energy deposits to the \mET{}. This is estimated by
varying the energy in each unclustered deposit by its resolution, and
propagating the result to the \mET{} calculation.

\subsection{Pileup Reweighting}\label{sec:syst_pileup}  % pileup

A systematic uncertainty associated with the pileup reweighting applied to simulated data
(see \cref{sec:pileup_modelling}) is created by altering the expected
minimum-bias cross section by \(±\SI{4.6}{\percent}\). This alters the
distribution of the number of primary vertices in simulated samples, allowing
the effect of having more or fewer pileup interactions to be evaluated.

\subsection{b~Tagging Scale Factors}  % bTag

The scale factors introduced in \cref{sec:b-tag_efficiency} are varied by
\(1σ\) uncertainty intervals provided by the \ac{cms} \ac{btv} group in order
to estimate the impact of \btag{}ging uncertainty on the analysis.

\subsection{Parton Density Functions}  % pdf

Uncertainties on the \acp{pdf} are propagated through the \tZq{} analysis by
creating a nuisance parameter in the signal extraction step representing a
\(±1σ\) change to the \acp{pdf}. This is in accordance with
\textls{\case{PDF4LHC}} recommendations\footnote{In most cases, per-event
weights provided with the sample were used to create the required shapes.
However, for the \tW{} and \tbarW{} samples in 2016, which were generated with
\textsc{Powheg Box~\case{V1}}, these weights were not included and were instead
generated by the \ac{lhapdf} library~\cite{lhapdf} using the
\textls{\case{NNPDF3.0}} \ac{pdf} set~\cite{nnpdf,Morton19}. }~\cite{PDF4LHC}.

\subsection{Perturbative Factorisation and Renormalisation Scales}\label{sec:syst_ME}  % ME

A systematic uncertainty was introduced to account for the choice of the
factorisation energy scale, \(μ_{\symup{F}}\), and renormalisation energy
scale, \(μ_{\symup{R}}\), when performing the \ac{me} calculations under
perturbative \ac{qcd} (see \cref{sec:event_generation}). The \(+1σ\) variation
is created by simultaneously halving the choices of \(μ_{\symup{F}}\) and
\(μ_{\symup{R}}\) from the nominal value, and the \(-1σ\) variation from
doubling the choices of \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) from the
nominal value.

\subsection{Non-Perturbative Factorisation and Renormalisation Scales}\label{sec:syst_scale}  % scale, isr, fsr

\begin{table}
    \centering
    \begin{threeparttable}
        \caption{Dedicated systematic samples used to estimate uncertainties in
            the 2016 \tZq{} analysis. In this table, the shorthand allowing
            `\Pt' to refer either to the top or antitop quark is
        suspended.}\label{tab:systematic_samples_2016}
        \footnotesize
        \begin{tabular}{llSl}
            \toprule
            \multicolumn{1}{c}{Process} & \multicolumn{1}{c}{Systematic} & \multicolumn{1}{c}{Events (\(×10⁶\))} & \multicolumn{1}{c}{Generator} \tabularnewline
            \midrule
            \midrule
            \tZq\tnote{\(^\ast\)}   & scale up      & 6.89  & \madnlo    \tabularnewline
            \tZq\tnote{\(^\ast\)}   & scale down    & 6.98  & \madnlo    \tabularnewline
            \midrule
            \tW                     & scale up      & 0.998 & \powhegbox \tabularnewline
            \tW                     & scale down    & 0.994 & \powhegbox \tabularnewline
            \midrule
            \tbarW                  & scale up      & 1.00  & \powhegbox \tabularnewline
            \tbarW                  & scale down    & 0.999 & \powhegbox \tabularnewline
            \midrule
            \(\Pt\Pq\) (t-channel)  & matching up   & 6.00  & \powhegbox \tabularnewline
            \(\Pt\Pq\) (t-channel)  & matching down & 6.00  & \powhegbox \tabularnewline
            \(\Pt\Pq\) (t-channel)  & scale up      & 5.71  & \powhegbox \tabularnewline
            \(\Pt\Pq\) (t-channel)  & scale down    & 5.95  & \powhegbox \tabularnewline
            \midrule
            \(\Ptb\Pq\) (t-channel) & matching up   & 4.00  & \powhegbox \tabularnewline
            \(\Ptb\Pq\) (t-channel) & matching down & 4.00  & \powhegbox \tabularnewline
            \(\Ptb\Pq\) (t-channel) & scale up      & 3.97  & \powhegbox \tabularnewline
            \(\Ptb\Pq\) (t-channel) & scale down    & 3.89  & \powhegbox \tabularnewline
            \midrule
            \ttbar                  & \glsfmtshort{isr} up     & 156   & \powhegbox \tabularnewline
            \ttbar                  & \glsfmtshort{isr} down   & 150   & \powhegbox \tabularnewline
            \ttbar                  & \glsfmtshort{fsr} up     & 153   & \powhegbox \tabularnewline
            \ttbar                  & \glsfmtshort{fsr} down   & 156   & \powhegbox \tabularnewline
            \ttbar                  & matching up              & 58.9  & \powhegbox \tabularnewline
            \ttbar                  & matching down            & 58.2  & \powhegbox \tabularnewline
            \bottomrule
        \end{tabular}
        \begin{tablenotes}
            \footnotesize
            \item[\(^\ast\)] Including \Ptb{} decays
        \end{tablenotes}
    \end{threeparttable}
\end{table}

The effect of the choice of \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) must also
be considered in the nonperturbative \ac{qcd} regime used when simulating
parton showers. In the 2016 analysis this was achieved through dedicated
systematic samples listed in \cref{tab:systematic_samples_2016}. The
`\acs{isr} up' and `\acs{fsr} down' samples consider the effects of
changing \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) in the \ac{isr} and \ac{fsr}
separately, while for the `scale up' and `scale down' samples the \ac{isr}
and \ac{fsr} are considered together. In the 2017 analysis dedicated samples
are no longer used, instead some nominal samples contain per-event weights used
to create the shapes required evaluate this systematic uncertainty. Samples
containing these weights are indicated in \cref{tab:mc_2017} and these weights
were used when available. The \(+1σ\) (\(-1σ\)) variation was created by
doubling (halving) the choices of \μF{} and \μR{}.

\subsection{Matching Threshold Energy}\label{sec:syst_hdamp}  % hdamp

\begin{table}
    \centering
    \begin{threeparttable}
        \caption{Dedicated systematic samples used to estimate uncertainties in
            the 2017 \tZq{} analysis.}\label{tab:systematic_samples_2017}
        \footnotesize
        \begin{tabular}{llSl}
            \toprule
            \multicolumn{1}{c}{Process} & \multicolumn{1}{c}{Systematic} & \multicolumn{1}{c}{Events (\(×10⁶\))} & \multicolumn{1}{c}{Generator} \tabularnewline
            \midrule
            \midrule
            \(\ttbar→\Pl\Pν\Pl\Pν\) & matching up   & 3.29  & \powhegbox \tabularnewline
            \(\ttbar→\Pl\Pν\Pl\Pν\) & matching down & 5.48  & \powhegbox \tabularnewline
            \midrule
            \(\ttbar→\Pl\Pν\Pq\Pq\) & matching up   & 24.0  & \powhegbox \tabularnewline
            \(\ttbar→\Pl\Pν\Pq\Pq\) & matching down & 26.8  & \powhegbox \tabularnewline
            \midrule
            \(\ttbar→\Pq\Pq\Pq\Pq\) & matching up   & 27.3  & \powhegbox \tabularnewline
            \(\ttbar→\Pq\Pq\Pq\Pq\) & matching down & 27.1  & \powhegbox \tabularnewline
            \bottomrule
        \end{tabular}
    \end{threeparttable}
\end{table}

Dedicated simulated samples were used to estimate the impact of the choice of
\emph{matching threshold energy}. This is the energy scale at which the
matching of particles described by perturbative and nonperturbative \ac{qcd} is
performed (see \cref{sec:event_generation}). These samples exist only for
certain \powhegbox{} samples: \ttbar{} and single top t"~channel in 2016 and
the three \ttbar{} decay modes in 2017. They are listed as `matching up' and
`matching down' in
\cref{tab:systematic_samples_2016,tab:systematic_samples_2017}.

The matching energy threshold in \powhegbox{} is defined as
\begin{equation}
    E_{\text{matching}} = \frac{h_{\text{damp}}²}{h_{\text{damp}}²+\pT²}
\end{equation}
where \pT{} is the transverse momentum of the object being matched and
nominally \(h_{\text{damp}} = 1.58m_{\Pt}\). The `matching down' and
`matching up' samples are created by reducing and increasing
\(h_{\text{damp}}\) by one standard deviation, respectively~\cite{hdamp}.

\section{Pre-Fit Impact of Systematic Uncertainties}

\afterpage{%
    \begin{landscape}
        \begin{table}[p]
            \centering
            \caption{Impact of systematic uncertainties on the normalisation of simulated samples in the signal region in 2016.}\label{tab:systematics_2016}
            \footnotesize
            \begin{tabular}{QlQcQcQcQcQcQcQcQc}
                \toprule
                \multicolumn{0}{c}{Uncertainty} & \multicolumn{0}{c}{Total} & \multicolumn{0}{c}{\tZq} & \multicolumn{0}{c}{\Zjets} & \multicolumn{0}{c}{\ttbar} & \multicolumn{0}{c}{\ttV} & \multicolumn{0}{c}{Single top} & \multicolumn{0}{c}{\VV} & \multicolumn{0}{c}{\VVV} \tabularnewline
                \multicolumn{0}{c}{(\(\ee \mathbin{/} \μμ\))} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} & \multicolumn{0}{c}{(\%)} \tabularnewline
                % \midrule
                % Statistical                                                      & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) \tabularnewline
                \midrule
                Luminosity                                                                              & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \)                             & \(± 2.5  \mathbin{/} ± 2.5 \) \tabularnewline
                Normalisation                                                                           & \(± 7.16 \mathbin{/} ± 7.14\)                             & —                                                         & \(±10.00 \mathbin{/} ±10.00\)                             & \(±10.00 \mathbin{/} ±10.00\)                             & \(± 8.56 \mathbin{/} ± 8.49\)                             & \(± 6.42 \mathbin{/} ± 6.42\)                             & \(± 7.10 \mathbin{/} ± 7.06\)                             & \(± 5.90 \mathbin{/} ± 5.78\) \tabularnewline
                Lepton efficiency                                                                       & \({}^{ +5.14}_{ -4.94} \mathbin{/} {}^{ +5.26}_{ +0.89}\) & \({}^{ +5.33}_{ -5.11} \mathbin{/} {}^{ +5.19}_{ +0.98}\) & \({}^{ +5.24}_{ -5.03} \mathbin{/} {}^{ +5.28}_{ +0.90}\) & \({}^{ +4.93}_{ -4.74} \mathbin{/} {}^{ +5.30}_{ +0.88}\) & \({}^{ +5.51}_{ -5.28} \mathbin{/} {}^{ +5.17}_{ +0.99}\) & \({}^{ +5.20}_{ -5.00} \mathbin{/} {}^{ +5.32}_{ +0.86}\) & \({}^{ +5.46}_{ -5.22} \mathbin{/} {}^{ +5.18}_{ +0.98}\) & \({}^{ +5.44}_{ -5.44} \mathbin{/} {}^{ +5.15}_{ -1.09}\) \tabularnewline
                Jet smearing                                                                            & \({}^{+16.27}_{-12.86} \mathbin{/} {}^{+17.56}_{-12.83}\) & \({}^{ +3.72}_{ -3.93} \mathbin{/} {}^{ +4.05}_{ -4.19}\) & \({}^{+20.76}_{-15.64} \mathbin{/} {}^{+21.84}_{-15.39}\) & \({}^{ +7.87}_{ -7.84} \mathbin{/} {}^{ +9.90}_{ -8.40}\) & \({}^{ -1.42}_{ +1.12} \mathbin{/} {}^{ +0.01}_{ -0.38}\) & \({}^{+10.80}_{ -8.01} \mathbin{/} {}^{+14.19}_{-11.01}\) & \({}^{ +9.11}_{ -9.58} \mathbin{/} {}^{+10.76}_{ -8.14}\) & \({}^{ +1.02}_{ -1.02} \mathbin{/} {}^{ +0.16}_{ -2.96}\) \tabularnewline
                \glsfmtshortpl{jec}                                                                     & \({}^{ +9.72}_{ -4.96} \mathbin{/} {}^{ +8.72}_{ -4.72}\) & \({}^{ +0.30}_{ -0.59} \mathbin{/} {}^{ +0.66}_{ -0.75}\) & \({}^{+12.90}_{ -5.73} \mathbin{/} {}^{+11.20}_{ -5.79}\) & \({}^{ +3.93}_{ -3.83} \mathbin{/} {}^{ +4.34}_{ -2.92}\) & \({}^{ -1.65}_{ +1.25} \mathbin{/} {}^{ -0.93}_{ +0.61}\) & \({}^{ +3.99}_{ -3.70} \mathbin{/} {}^{ +6.50}_{ -4.08}\) & \({}^{ +3.26}_{ -3.18} \mathbin{/} {}^{ +3.60}_{ -2.09}\) & \({}^{ +1.36}_{ -0.68} \mathbin{/} {}^{ +0.31}_{ -2.18}\) \tabularnewline
                Pileup reweighting                                                                     & \({}^{ +1.59}_{ -1.31} \mathbin{/} {}^{ +2.51}_{ -2.47}\) & \({}^{ +0.63}_{ -0.59} \mathbin{/} {}^{ +0.24}_{ -0.26}\) & \({}^{ +2.26}_{ -1.83} \mathbin{/} {}^{ +3.45}_{ -3.42}\) & \({}^{ +0.25}_{ -0.26} \mathbin{/} {}^{ +0.74}_{ -0.66}\) & \({}^{ +0.50}_{ -0.53} \mathbin{/} {}^{ -0.29}_{ +0.29}\) & \({}^{ +1.26}_{ -1.26} \mathbin{/} {}^{ +1.11}_{ -1.18}\) & \({}^{ +1.02}_{ -0.97} \mathbin{/} {}^{ +1.60}_{ -1.43}\) & \({}^{ +0.00}_{ -0.34} \mathbin{/} {}^{ -0.47}_{ +0.00}\) \tabularnewline
                \btag{}ging                                                                             & \({}^{ +7.59}_{ -7.59} \mathbin{/} {}^{ +4.81}_{ -4.81}\) & \({}^{+12.43}_{-12.43} \mathbin{/} {}^{ +3.60}_{ -3.60}\) & \({}^{ +6.41}_{ -6.41} \mathbin{/} {}^{ +5.34}_{ -5.34}\) & \({}^{+10.10}_{-10.10} \mathbin{/} {}^{ +3.69}_{ -3.69}\) & \({}^{+15.26}_{-15.26} \mathbin{/} {}^{ +3.84}_{ -3.84}\) & \({}^{ +6.24}_{ -6.24} \mathbin{/} {}^{ +3.42}_{ -3.42}\) & \({}^{ +7.33}_{ -7.33} \mathbin{/} {}^{ +6.28}_{ -6.28}\) & \({}^{+11.22}_{-11.22} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \glsfmtshort{pdf}                                                                       & \({}^{ +7.98}_{ -7.96} \mathbin{/} {}^{ +7.39}_{ -7.38}\) & \({}^{+15.93}_{-15.93} \mathbin{/} {}^{+16.14}_{-16.14}\) & \({}^{+10.30}_{-10.30} \mathbin{/} {}^{ +9.52}_{ -9.52}\) & \({}^{ +2.38}_{ -2.38} \mathbin{/} {}^{ +2.43}_{ -2.43}\) & \({}^{ +6.90}_{ -6.90} \mathbin{/} {}^{ +6.85}_{ -6.85}\) & \({}^{ +8.87}_{ -8.04} \mathbin{/} {}^{ +9.04}_{ -8.07}\) & \({}^{ +7.72}_{ -7.72} \mathbin{/} {}^{ +8.27}_{ -8.27}\) & \({}^{ +4.42}_{ -4.42} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{me})                        & \({}^{+10.66}_{ -9.48} \mathbin{/} {}^{ +9.95}_{ -8.99}\) & \({}^{ +7.31}_{ -7.40} \mathbin{/} {}^{ +6.76}_{ -7.06}\) & \({}^{ +8.86}_{ -8.05} \mathbin{/} {}^{ +7.91}_{ -7.32}\) & \({}^{+15.73}_{-13.56} \mathbin{/} {}^{+15.11}_{-13.27}\) & \({}^{+11.37}_{-11.85} \mathbin{/} {}^{+10.62}_{-11.50}\) & \({}^{ +1.02}_{ -0.97} \mathbin{/} {}^{ +1.00}_{ -0.95}\) & \({}^{ +9.79}_{ -7.76} \mathbin{/} {}^{ +9.90}_{ -7.75}\) & \({}^{ +8.84}_{ -6.80} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{isr})                       & \({}^{ +4.19}_{ +1.20} \mathbin{/} {}^{ -1.88}_{ -4.04}\) & —                                                         & —                                                         & \({}^{+15.22}_{ +4.34} \mathbin{/} {}^{ -6.47}_{-13.95}\) & —                                                         & —                                                         & —                                                         & —                                                       \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{fsr})                       & \({}^{ -0.90}_{ +4.03} \mathbin{/} {}^{ -5.66}_{ -1.19}\) & —                                                         & —                                                         & \({}^{ -3.28}_{+14.63} \mathbin{/} {}^{-19.54}_{ -4.11}\) & —                                                         & —                                                         & —                                                         & —                                                       \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{isr} and \glsfmtshort{fsr}) & \({}^{ -0.08}_{ -0.29} \mathbin{/} {}^{ +0.04}_{ -0.13}\) & \({}^{+19.19}_{ -0.80} \mathbin{/} {}^{+25.87}_{-13.38}\) & —                                                         & —                                                         & —                                                         & \({}^{-14.12}_{-20.83} \mathbin{/} {}^{ -8.04}_{-16.39}\) & —                                                         & —                                                       \tabularnewline
                \glsfmtshort{me}–\glsfmtshort{ps} matching scale  (\(h_{\text{damp}}\))                 & \({}^{ +2.03}_{ +0.23} \mathbin{/} {}^{ -1.89}_{ -4.40}\) & —                                                         & —                                                         & \({}^{ +7.41}_{ +0.74} \mathbin{/} {}^{ -6.54}_{-15.16}\) & —                                                         & \({}^{ -0.75}_{ +1.62} \mathbin{/} {}^{ +0.66}_{ -0.25}\) & —                                                         & —                                                       \tabularnewline
                \mET                                                                                    & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \glsfmtshort{npl} estimate                                                              & \({}^{ +0.11}_{ -0.11} \mathbin{/} {}^{ +0.13}_{ -0.13}\) & —                                                         & —                                                         & —                                                         & —                                                         & —                                                         & —                                                         & — \tabularnewline
                \midrule
                Total systematic uncertainty                                                            & \({}^{+26.51}_{-22.41} \mathbin{/} {}^{+26.30}_{-21.13}\) & \({}^{+29.65}_{-22.63} \mathbin{/} {}^{+32.22}_{-22.97}\) & \({}^{+31.02}_{-24.99} \mathbin{/} {}^{+30.50}_{-23.71}\) & \({}^{+29.33}_{-27.02} \mathbin{/} {}^{+31.08}_{-28.70}\) & \({}^{+22.90}_{-23.05} \mathbin{/} {}^{+16.75}_{-16.55}\) & \({}^{+22.96}_{-26.33} \mathbin{/} {}^{+21.91}_{-23.11}\) & \({}^{+19.76}_{-18.98} \mathbin{/} {}^{+20.50}_{-17.25}\) & \({}^{+17.24}_{-16.25} \mathbin{/} {}^{+13.36}_{-12.10}\) \tabularnewline
                \bottomrule
            \end{tabular}
        \end{table}
    \end{landscape}
}

\afterpage{%
    \begin{landscape}
        \begin{table}[p]
            \centering
            \caption{Impact of systematic uncertainties on the normalisation of simulated samples in the signal region 2017.}\label{tab:systematics_2017}
            \footnotesize
            \begin{tabular}{QlQcQcQcQcQcQcQcQc}
                \toprule
                \multicolumn{1}{c}{Uncertainty} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{\tZq} & \multicolumn{1}{c}{\Zjets} & \multicolumn{1}{c}{\ttbar} & \multicolumn{1}{c}{\ttV} & \multicolumn{1}{c}{Single top} & \multicolumn{1}{c}{\VV} & \multicolumn{1}{c}{\VVV} \tabularnewline
                \multicolumn{1}{c}{(\(\ee \mathbin{/} \μμ\))} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} & \multicolumn{1}{c}{(\%)} \tabularnewline
                % \midrule
                % Statistical                                                      & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) & \(± 0.00 \mathbin{/} ± 0.00\) \tabularnewline
                \midrule
                Luminosity                                                                              & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \)                             & \(± 2.3  \mathbin{/} ± 2.3 \) \tabularnewline
                Normalisation                                                                           & \(± 7.07 \mathbin{/} ± 7.04\)                             & —                                                         & \(±10.00 \mathbin{/} ± 10.00\)                             & \(±10.00 \mathbin{/} ± 10.00\)                             & \(± 7.20 \mathbin{/} ± 7.17\)                             & \(± 6.34 \mathbin{/} ± 6.34\)                             & \(± 8.62 \mathbin{/} ± 8.59\)                             & \(± 6.36 \mathbin{/} ± 6.28\) \tabularnewline
                Lepton efficiency                                                                       & \({}^{ +4.45}_{ -4.29} \mathbin{/} {}^{ +1.68}_{ -1.67}\) & \({}^{ +4.75}_{ -4.57} \mathbin{/} {}^{ +1.70}_{ -1.67}\) & \({}^{ +4.58}_{ -4.42} \mathbin{/} {}^{ +1.68}_{ -1.67}\) & \({}^{ +4.20}_{ -4.06} \mathbin{/} {}^{ +1.69}_{ -1.67}\) & \({}^{ +4.90}_{ -4.71} \mathbin{/} {}^{ +1.66}_{ -1.47}\) & \({}^{ +4.40}_{ -4.26} \mathbin{/} {}^{ +1.67}_{ -1.66}\) & \({}^{ +5.05}_{ -4.86} \mathbin{/} {}^{ +1.69}_{ -1.67}\) & \({}^{ +5.40}_{ -5.04} \mathbin{/} {}^{ +1.62}_{ -1.62}\) \tabularnewline
                Jet smearing                                                                            & \({}^{+20.39}_{-16.28} \mathbin{/} {}^{+21.69}_{-17.91}\) & \({}^{ +4.85}_{ -4.60} \mathbin{/} {}^{ +4.59}_{ -5.34}\) & \({}^{+25.31}_{-19.25} \mathbin{/} {}^{+27.82}_{-22.06}\) & \({}^{+11.69}_{-11.66} \mathbin{/} {}^{+12.03}_{-11.31}\) & \({}^{ +1.17}_{ -1.60} \mathbin{/} {}^{ +2.68}_{ -3.36}\) & \({}^{+14.84}_{-13.19} \mathbin{/} {}^{+18.34}_{-18.98}\) & \({}^{+15.07}_{-10.90} \mathbin{/} {}^{+12.59}_{-11.98}\) & \({}^{ +6.47}_{ -9.35} \mathbin{/} {}^{ +4.71}_{ -6.19}\) \tabularnewline
                \glsfmtshortpl{jec}                                                                     & \({}^{+10.86}_{-13.06} \mathbin{/} {}^{+11.46}_{-14.06}\) & \({}^{ +0.25}_{ -1.29} \mathbin{/} {}^{ +0.81}_{ -1.75}\) & \({}^{+14.05}_{-16.31} \mathbin{/} {}^{+15.22}_{-18.37}\) & \({}^{ +5.11}_{ -7.59} \mathbin{/} {}^{ +5.44}_{ -7.21}\) & \({}^{ +0.05}_{ -0.17} \mathbin{/} {}^{ +1.63}_{ -1.22}\) & \({}^{ +8.53}_{-11.43} \mathbin{/} {}^{ +9.06}_{-13.31}\) & \({}^{ +6.59}_{ -6.88} \mathbin{/} {}^{ +6.24}_{ -7.41}\) & \({}^{ +3.24}_{ -5.40} \mathbin{/} {}^{ +0.81}_{ -2.56}\) \tabularnewline
                Pileup reweighting                                                                     & \({}^{ +0.12}_{ -0.12} \mathbin{/} {}^{ +0.20}_{ -0.20}\) & \({}^{ -0.04}_{ +0.04} \mathbin{/} {}^{ +0.00}_{ -0.00}\) & \({}^{ +0.17}_{ -0.17} \mathbin{/} {}^{ +0.27}_{ -0.27}\) & \({}^{ +0.02}_{ -0.02} \mathbin{/} {}^{ +0.08}_{ -0.08}\) & \({}^{ -0.01}_{ +0.01} \mathbin{/} {}^{ +0.03}_{ -0.03}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.16}_{ -0.16}\) & \({}^{ +0.10}_{ -0.10} \mathbin{/} {}^{ +0.22}_{ -0.22}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \btag{}ging                                                                             & \({}^{ +5.03}_{ -5.03} \mathbin{/} {}^{ +5.03}_{ -5.03}\) & \({}^{ +3.70}_{ -3.70} \mathbin{/} {}^{ +3.71}_{ -3.71}\) & \({}^{ +5.48}_{ -5.48} \mathbin{/} {}^{ +5.51}_{ -5.51}\) & \({}^{ +3.86}_{ -3.86} \mathbin{/} {}^{ +3.86}_{ -3.86}\) & \({}^{ +4.35}_{ -4.35} \mathbin{/} {}^{ +4.25}_{ -4.25}\) & \({}^{ +4.33}_{ -4.33} \mathbin{/} {}^{ +3.75}_{ -3.75}\) & \({}^{ +8.07}_{ -8.07} \mathbin{/} {}^{ +8.36}_{ -8.36}\) & \({}^{ +6.12}_{ -6.12} \mathbin{/} {}^{ +7.40}_{ -7.40}\) \tabularnewline
                \glsfmtshort{pdf}                                                                       & \({}^{ +0.82}_{ -0.82} \mathbin{/} {}^{ +0.78}_{ -0.78}\) & \({}^{+20.64}_{-20.64} \mathbin{/} {}^{+20.28}_{-20.28}\) & \({}^{ +0.95}_{ -0.95} \mathbin{/} {}^{ +0.94}_{ -0.94}\) & \({}^{ +0.19}_{ -0.19} \mathbin{/} {}^{ +0.19}_{ -0.19}\) & \({}^{ +0.79}_{ -0.79} \mathbin{/} {}^{ +0.78}_{ -0.78}\) & \({}^{ +0.19}_{ -0.19} \mathbin{/} {}^{ +0.26}_{ -0.26}\) & \({}^{ +1.05}_{ -1.05} \mathbin{/} {}^{ +0.93}_{ -0.93}\) & \({}^{ +4.68}_{ -4.68} \mathbin{/} {}^{ +3.50}_{ -3.50}\) \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{me})                        & \({}^{ +9.40}_{ -8.78} \mathbin{/} {}^{ +9.10}_{ -8.69}\) & \({}^{ +7.62}_{ -7.95} \mathbin{/} {}^{ +7.31}_{ -7.73}\) & \({}^{ +7.18}_{ -7.12} \mathbin{/} {}^{ +5.95}_{ -6.35}\) & \({}^{+14.97}_{-13.20} \mathbin{/} {}^{+14.90}_{-13.20}\) & \({}^{+10.33}_{-10.37} \mathbin{/} {}^{ +9.75}_{ -9.81}\) & \({}^{ +5.57}_{ -4.04} \mathbin{/} {}^{ +6.72}_{ -4.87}\) & \({}^{+11.06}_{ -8.65} \mathbin{/} {}^{+12.17}_{ -9.37}\) & \({}^{ +7.19}_{ -5.76} \mathbin{/} {}^{ +7.00}_{ -5.52}\) \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{isr})                       & \({}^{ -0.72}_{ +0.87} \mathbin{/} {}^{ -0.81}_{ +0.98}\) & \({}^{ -0.47}_{ +0.36} \mathbin{/} {}^{ -0.58}_{ +0.55}\) & —                                                         & \({}^{ -2.52}_{ +3.05} \mathbin{/} {}^{ -2.33}_{ +2.79}\) & \({}^{ +0.83}_{ -1.23} \mathbin{/} {}^{ +0.69}_{ -1.07}\) & \({}^{ +3.85}_{ -4.59} \mathbin{/} {}^{ -4.12}_{ +5.13}\) & \({}^{ -0.08}_{ +0.09} \mathbin{/} {}^{ -0.81}_{ +1.02}\) & —                                                       \tabularnewline
                \(μ_{\symup{F}}\) and \(μ_{\symup{R}}\) scale (\glsfmtshort{fsr})                       & \({}^{ +0.26}_{ -0.48} \mathbin{/} {}^{ +0.40}_{ -0.72}\) & \({}^{ +0.51}_{ -0.65} \mathbin{/} {}^{ +1.93}_{ -3.13}\) & —                                                         & \({}^{ +0.98}_{ -1.66} \mathbin{/} {}^{ +1.17}_{ -2.13}\) & \({}^{ +0.28}_{ -0.12} \mathbin{/} {}^{ +0.87}_{ -1.79}\) & \({}^{ -2.23}_{ -1.22} \mathbin{/} {}^{ +0.93}_{ +0.40}\) & \({}^{ -0.03}_{ -0.17} \mathbin{/} {}^{ -0.15}_{ +0.06}\) & —                                                       \tabularnewline
                \glsfmtshort{me}–\glsfmtshort{ps} matching scale (\(h_{\text{damp}}\))                  & \({}^{ +0.80}_{ -1.92} \mathbin{/} {}^{ +1.22}_{ -1.53}\) & —                                                         & —                                                         & \({}^{ +2.93}_{ -6.98} \mathbin{/} {}^{ +3.81}_{ -4.74}\) & —                                                         & —                                                         & —                                                         & —                                                       \tabularnewline
                \mET                                                                                    & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ +0.00}\) & \({}^{ +0.00}_{ -0.00} \mathbin{/} {}^{ +0.00}_{ -0.00}\) \tabularnewline
                \glsfmtshort{npl} estimate                                                              & \({}^{ +0.35}_{ -0.35} \mathbin{/} \text{—}\)             & —                                                         & —                                                         & —                                                         & —                                                         & —                                                         & —                                                         & — \tabularnewline
                % \mET & & & & & & \tabularnewline
                \midrule
                Total systematic uncertainty                                                            & \({}^{+26.92}_{-24.84} \mathbin{/} {}^{+27.74}_{-26.08}\) & \({}^{+23.51}_{-23.61} \mathbin{/} {}^{+22.64}_{-23.12}\) & \({}^{+32.35}_{-29.03} \mathbin{/} {}^{+34.36}_{-31.69}\) & \({}^{+23.25}_{-23.78} \mathbin{/} {}^{+23.25}_{-22.63}\) & \({}^{+14.47}_{-14.50} \mathbin{/} {}^{+13.37}_{-13.62}\) & \({}^{+20.67}_{-20.64} \mathbin{/} {}^{+23.32}_{-25.50}\) & \({}^{+23.76}_{-20.26} \mathbin{/} {}^{+22.34}_{-20.98}\) & \({}^{+15.44}_{-16.73} \mathbin{/} {}^{+13.55}_{-13.68}\) \tabularnewline
                \bottomrule
            \end{tabular}
        \end{table}
    \end{landscape}
}

The influence on the overall yield of simulated samples in the signal region
for each systematic uncertainty featured in this chapter is shown in
\cref{tab:systematics_2016} for 2016 and \cref{tab:systematics_2017} for 2017.
In the case of shape uncertainties it should be noted that this measure may
belie the magnitude of their influence on the underlying shapes. For example,
the effect on the overall normalisation of the systematic uncertainty in \mET{}
is listed as zero in \cref{tab:systematics_2016,tab:systematics_2017}  (as no
requirements are placed on \mET{} in the signal region). That is not to say
that this uncertainty has no effect on the \mET{} distribution, only that
the overall normalisation remains unchanged.

\section{Correlation of systematic uncertainties}\label{sec:systematic_correlation}

In the \tZq{} analysis the sources of systematic uncertainty are assumed to be
\SI{100}{\percent} uncorrelated with each other. This will be used to build the
likelihood model used in signal extraction, as described in
\cref{sec:signal_extraction}.

Across years, the same source of systematic uncertainty is either treated as
\SI{100}{\percent} correlated or \SI{100}{\percent} uncorrelated. Those treated
as \SI{100}{\percent} correlated are:
\begin{itemize}
    \item the uncertainty in cross-section normalisation for each background
        process
    \item the uncertainty associated with the pileup reweighting applied to
        simulated data
    \item all uncertainties stemming from the choice of \μF{} and \μR
    \item the uncertainty from the choice of matching threshold energy
        (\(h_{\text{damp}}\)).
\end{itemize}
Treating these uncertainties as correlated between data-taking eras is the
follows the latest recommendations of the \acs{cms} collaboration.

\section{Unresolved Issues in Leading Order Samples}

In 2017 simulated samples generated at \ac{lo}, there are unresolved issues in
the per-event weights used to determine the effect of some systematic
uncertainties. The affected systematic uncertainties are those associated with
\begin{itemize}
    \item the choice of factorisation and renormalisation scales for
        perturbative \ac{qcd} simulation in the \tHq{}, \tWZ{}, and \ttγ{}
        samples
    \item the \acp{pdf} in \tWZ{} and \ttγ{} samples
    \item the choice of factorisation and renormalisation scales for
        nonperturbative \ac{qcd} simulation in the \ttγ{} sample.

\end{itemize}
When using these weights, the effect on the underlying distributions of a
\(±1σ\) variation in the systematic uncertainty is increased by \({>}1000×\).
The reason for this was not identified before the completion of this thesis.
These systematic uncertainties are, therefore, excluded from further
consideration; the excluded systematics are expected have a
\({≲}±\SI{0.01}{\percent}\) effect on the overall normalisation in the signal
region, judging by their effect in 2016 data.

\end{document}
